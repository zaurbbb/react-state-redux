import React, {
  useEffect,
  useState
} from "react";
import {
  useNavigate,
  useParams
} from "react-router-dom";
import { API } from "../../../api";
import UserCard from "../../../components/UserCard";

const UserModule = () => {
  const { userId } = useParams();
  const navigate = useNavigate();
  const [userData, setUserData] = useState({});

  useEffect(() => {
    async function fetchUser() {
      const response = await API.get(`/community/${userId}`);
      setUserData(response.data);
    }

    fetchUser()
      .catch(() => {
        navigate("/notFound")
      });
  }, [navigate, userId]);

  return (
    <section className="app-section">
      <UserCard
        id={userData.id}
        avatar={userData.avatar}
        firstName={userData.firstName}
        lastName={userData.lastName}
        position={userData.position}
      />
    </section>
  );
};

export default UserModule;
