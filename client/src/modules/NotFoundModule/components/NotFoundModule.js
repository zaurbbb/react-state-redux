import React from "react";
import { Link } from "react-router-dom";

const NotFoundModule = () => {
  return (
    <section className="app-section app-section--not-found app-section--secondary ">
      <p>
        Page not found
      </p>
      <p>
        Looks like you've followed a brojen link or entered a URL that doesn't exist on this site
      </p>
      <Link to="/">← Back to our site  </Link>

    </section>
  );
};

export default NotFoundModule;
