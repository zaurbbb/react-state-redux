import React, { useState } from "react";
import { API } from "../../../api";
import { useDispatch } from "react-redux";
import { setIsSubscribed } from "../../../redux";

const UnsubscribeForm = () => {
  const [isButtonDisabled, setIsButtonDisabled] = useState(false);
  const dispatch = useDispatch();
  function handleSubmit(e) {
    e.preventDefault();
    setTimeout(async () => {
      setIsButtonDisabled(true);
      const response = await API.post("/unsubscribe");

      if (response.status === 200) {
        dispatch(setIsSubscribed(false));
      } else {
        // setError(response.data.error);
      }
      setIsButtonDisabled(false);
    }, 1000);
  }

  return (
    <form
      className="app-section__form"
      onSubmit={handleSubmit}
    >
      <input
        type="submit"
        className="app-section__button app-section__button--subscribe"
        value="Unsubscribe"
        disabled={isButtonDisabled}
      />
    </form>
  );
};

export default UnsubscribeForm;
