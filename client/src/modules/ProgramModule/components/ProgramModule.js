import React, { useState } from "react";
import SubscribeForm from "./SubscribeForm";
import UnsubscribeForm from "./UnsubscribeForm";
import ErrorMessage from "./ErrorMessage";
import { useSelector } from "react-redux";
import { subscribeSelector } from "../../../redux";

const ProgramModule = () => {
  const [error, setError] = useState("");
  const showError = error && <ErrorMessage message={error} />
  const { isSubscribed } = useSelector(subscribeSelector);
  const formContent =
          !isSubscribed ?
            <SubscribeForm setError={setError} /> :
            <UnsubscribeForm setError={setError}/>;

  return (
    <section className="app-section app-section--image-people">
      <h2 className="app-title">Join Our Program</h2>
      <h3 className="app-subtitle">
        Sed do eiusmod tempor incididunt <br />
        ut labore et dolore magna aliqua.<br />
        {showError}
      </h3>
      {formContent}
    </section>
  );
};

export default ProgramModule;
