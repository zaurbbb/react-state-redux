import React, { useState } from "react";
import emailValidator from "../../../helpers/emailValidator";
import { useInput } from "../../../hooks/useInput";
import { API } from "../../../api";
import { useDispatch } from "react-redux";
import { setIsSubscribed } from "../../../redux";

const SubscribeForm = ({ setError }) => {
  const email = useInput("");
  const [isButtonDisabled, setIsButtonDisabled] = useState(false);
  const dispatch = useDispatch();

  function handleSubmit(e) {
    e.preventDefault();
    if (emailValidator(email.value)) {
      setTimeout(async () => {
        setIsButtonDisabled(true);
        await API
          .post("/subscribe", { email: email.value })
          .then(() => {
            dispatch(setIsSubscribed(true));
          })
          .catch(error => {
            const errorStatus = error.response.status;
            const errorMessage = error.response.data.error;
            if (errorStatus === 422) {
              alert(errorMessage)
            } else {
              setError(errorStatus);
            }
          });
        setIsButtonDisabled(false);
      }, 1000);
    } else {
      setError("Please enter a valid email address");
    }
  }

  return (
    <form
      className="app-section__form"
      onSubmit={handleSubmit}
    >

      <input
        className="app-section__input"
        placeholder="Email"
        {...email}
      />
      <input
        type="submit"
        className="app-section__button app-section__button--subscribe"
        value="Subscribe"
        disabled={isButtonDisabled}
      />
    </form>
  );
};

export default SubscribeForm;
