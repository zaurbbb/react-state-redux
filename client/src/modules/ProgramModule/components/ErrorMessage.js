import React from "react";

const ErrorMessage = ({ message }) => {
  return (
    <span className="app-section__error-message">
      {message}
    </span>
  );
};

export default ErrorMessage;
