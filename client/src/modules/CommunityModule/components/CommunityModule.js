import React, { useEffect } from "react";
import {
  useDispatch,
  useSelector
} from "react-redux";

import {
  getUsers,
  setIsHidden,
  usersSelector,
  visibilitySelector
} from "../../../redux";

import UserCard from "../../../components/UserCard";

import SectionContent from "./SectionContent";

const CommunityModule = () => {
  const dispatch = useDispatch();
  const { users, isFetching } = useSelector(usersSelector);
  const { isHidden } = useSelector(visibilitySelector);
  const hideButtonText = isHidden ? "Show section" : "Hide section";

  useEffect(() => {
    dispatch(getUsers());
  }, [dispatch]);


  if (isFetching) {
    return <h2>Loading...</h2>;
  }

  const mappedUsers = users.map((person) => {
    const id = person.id;
    return (
      <UserCard
        key={id}
        id={id}
        avatar={person.avatar}
        firstName={person.firstName}
        lastName={person.lastName}
        position={person.position}
      />
    );
  });

  const showUsers = !isFetching ? mappedUsers : <h2>Loading</h2>;
  const sectionContent = !isHidden ? <SectionContent showUsers={showUsers} /> : null;

  function handleHideSection() {
    dispatch(setIsHidden(!isHidden));
  }

  return (
    <section className="app-section">
      <h2
        className="app-title"
        style={{ width: "100%" }}
      >
        <p>
          Big Community of <br />
          People Like You
        </p>
        <button
          className="app-section__button--hide"
          onClick={handleHideSection}
        >
          {hideButtonText}
        </button>
      </h2>

      {sectionContent}

    </section>
  );
};

export default CommunityModule;
