import React from "react";

const SectionContent = ({ showUsers }) => {
  return (
    <>
      <h3 className="app-subtitle">
        We’re proud of our products, and we’re really excited<br />
        when we get feedback from our users.
      </h3>
      <div className="app-section__users-list">
        {showUsers}
      </div>
    </>
  );
};

export default SectionContent;
