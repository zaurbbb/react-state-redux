import React from "react";
import { Link } from "react-router-dom";

const HeaderModule = () => {
  return (
    <header className="app-header">
      <Link
        className="app-header__logo"
        to="/"
      >
        Project
      </Link>

      <nav className="app-header__nav">
        <ul className="app-header__nav-list">
          <li className="app-header__nav-list-item"><Link to="/community">Community</Link></li>
          <li className="app-header__nav-list-item"><Link to="/">Projects</Link></li>
          <li className="app-header__nav-list-item"><Link to="/">About us</Link></li>
          <li className="app-header__nav-list-item"><Link to="/">Stories</Link></li>
        </ul>
      </nav>

      <div className="app-header__nav-menu-button">
        <div className="app-header__nav-menu-button-icon">
          <span></span>
          <span></span>
          <span></span>
        </div>
      </div>
    </header>
  );
};

export default HeaderModule;
