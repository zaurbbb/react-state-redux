import React from "react";

import yourLogoHere from "../../../assets/your-logo-here.png";

const GreetingModule = () => {
  return (
    <section className="app-section app-section--image-overlay app-section--image-peak">
      <img
        src={yourLogoHere}
        alt="yourLogoHere"
        className="app-logo"
      />

      <h1 className="app-title">
        Your Headline <br /> Here
      </h1>
      <h2 className="app-subtitle">
        Lorem ipsum dolor sit amet, consectetur <br />adipiscing elit sed do eiusmod.
      </h2>
    </section>
  );
};

export default GreetingModule;
