import React from "react";

import yourLogoFooter from "../../../assets/your-logo-footer.png";

const FooterModule = () => {
  return (
    <footer className="app-footer">
      <div className="app-footer__logo">
        <img
          src={yourLogoFooter}
          alt="yourLogoFooter"
          className="app-logo app-logo--small"
        /> Project
      </div>

      <address className="app-footer__address">
        123 Street,<br />
        Anytown, USA 12345
      </address>

      <a
        className="app-footer__email"
        href="mailto:hello@website.com"
      >
        hello@website.com
      </a>

      <p className="app-footer__rights">
        &copy; 2021 Project. All rights reserved
      </p>
    </footer>
  );
};

export default FooterModule;
