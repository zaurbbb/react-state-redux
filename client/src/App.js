import React from "react";

import "./styles/style.css";
import "./styles/normalize.css";
import AppRouter from "./AppRouter";


const App = () => {
  return (
    <main id="app-container">
      <AppRouter />
    </main>
  );
};

export default App;
