import React from "react";
import { Link } from "react-router-dom";

const UserCard = ({
  id,
  avatar,
  firstName,
  lastName,
  position
}) => {
  const fullName = `${firstName} ${lastName}`;
  const link = `/community/${id}`;
  return (
    <div className="card">
      <img
        className="card__img"
        src={avatar}
        alt="avatar"
      />
      <p className="card__description">
        Lorem ipsum dolor sit amet, <br />
        consectetur adipiscing elit, sed do <br />
        eiusmod tempor incididunt ut <br /> labore et dolor. <br /><br />
      </p>
      <Link to={link}>
        <h3 className="card__full-name">
          {fullName}
        </h3>
      </Link>
      <h3 className="card__company-name">
        {position}
      </h3>
    </div>
  );
};

export default UserCard;
