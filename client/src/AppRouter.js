import React from "react";
import {
  Navigate,
  Route,
  Routes
} from "react-router-dom";

import { appRoutes } from "./routes";

const AppRouter = () => {
  return (
    <Routes>
      {appRoutes.map(route =>
        <Route
          key={route.path}
          element={route.component}
          path={route.path}
        />
      )}
      <Route
        path="*"
        element={
          <Navigate
            to="/notFound"
            replace
          />
        }
      />
    </Routes>
  );
};

export default AppRouter;
