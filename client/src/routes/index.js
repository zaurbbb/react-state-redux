import MainPage from "../pages/MainPage";
import NotFoundPage from "../pages/NotFoundPage";
import CommunityPage from "../pages/CommunityPage";
import UserPage from "../pages/UserPage";

export const appRoutes = [
  {
    path: "/",
    component: <MainPage />,
  },
  {
    path: "/community",
    component: <CommunityPage />,
  },
  {
    path: "/community/:userId",
    component: <UserPage />,
  },
  {
    path: "/notFound",
    component: <NotFoundPage />,
  }
];
