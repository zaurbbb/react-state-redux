import { SET_IS_SUBSCRIBED } from "./subscribeConsts";

const initialState = {
  isSubscribed: false,
}

function subscribeReducer(state = initialState, action) {
  switch (action.type) {
    case SET_IS_SUBSCRIBED:
      return {
        ...state,
        isSubscribed: action.payload,
      }
    default:
      return state;
  }
}

export default subscribeReducer;
