import { SET_IS_SUBSCRIBED } from "./subscribeConsts";

export const setIsSubscribed = (bool) => ({
  type: SET_IS_SUBSCRIBED,
  payload: bool
});
