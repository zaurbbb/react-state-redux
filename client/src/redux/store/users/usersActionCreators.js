import { API } from "../../../api";
import {
  setIsFetching,
  setUsers
} from "./usersActions";

export function getUsers() {
  return async (dispatch) => {
    dispatch(setIsFetching(true));
    try {
      const response = await API.get("/community");
      console.log("response", response.data);
      dispatch(setUsers(response.data));
    } catch (e) {
      const err = e?.response?.data?.message;
      console.log("err", err);
    } finally {
      dispatch(setIsFetching(false));
    }
  }
}
