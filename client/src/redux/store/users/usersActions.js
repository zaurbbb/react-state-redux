import {
  SET_IS_FETCHING,
  SET_USERS
} from "./usersConsts";

export const setUsers = (data) => ({
  type: SET_USERS,
  payload: data
});

export const setIsFetching = (bool) => ({
  type: SET_IS_FETCHING,
  payload: bool
});

