import {
  SET_IS_FETCHING,
  SET_USERS
} from "./usersConsts";

const initialState = {
  users: [],
  isFetching: false,
};

function usersReducer(state = initialState, action) {
  switch (action.type) {
    case SET_USERS:
      return {
        ...state,
        users: action.payload,
      }
    case SET_IS_FETCHING:
      return {
        ...state,
        isFetching: action.payload,
      }
    default:
      return state;
  }
}

export default usersReducer;
