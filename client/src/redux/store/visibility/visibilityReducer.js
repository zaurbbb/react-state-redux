import { SET_IS_HIDDEN } from "./visibilityConsts";

const initialState = {
  isHidden: false,
}

function visibilityReducer(state = initialState, action) {
  switch (action.type) {
    case SET_IS_HIDDEN:
      return {
        ...state,
        isHidden: action.payload,
      }
    default:
      return state;
  }
}

export default visibilityReducer;
