import { SET_IS_HIDDEN } from "./visibilityConsts";

export const setIsHidden = (bool) => ({
  type: SET_IS_HIDDEN,
  payload: bool
});
