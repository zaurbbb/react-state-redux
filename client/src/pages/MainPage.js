import React from "react";

import { HeaderModule } from "../modules/HeaderModule";
import { GreetingModule } from "../modules/GreetingModule";
import { ArticleModule } from "../modules/ArticleModule";
import { CultureModule } from "../modules/CultureModule";
import { ProgramModule } from "../modules/ProgramModule";
import { FooterModule } from "../modules/FooterModule";

const MainPage = () => {
  return (
    <>
      <HeaderModule />
      <GreetingModule />
      <ArticleModule />
      <CultureModule />
      <ProgramModule />
      <FooterModule />
    </>
  );
};

export default MainPage;
