import React from "react";

import { NotFoundModule } from "../modules/NotFoundModule";

const NotFoundPage = () => {
  return (
    <NotFoundModule />
  );
};

export default NotFoundPage;
