import React from "react";
import { CommunityModule } from "../modules/CommunityModule";

const CommunityPage = () => {
  return (
    <CommunityModule />
  );
};

export default CommunityPage;
