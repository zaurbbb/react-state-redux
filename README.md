- Запустить тесты с coverage: ```npm run test```

- В ветке state находится решение задания "Mastering State in React"
- В ветке redux находится решение задания "Mastering Redux in React"
- В ветке router-task находится решение задания "Mastering React Routing"
- В ветке testes-task находится решение задания "Mastering Redux Testing"

# Как запустить проект?

- Сначала заходим в папку server

```
cd server
```

- Устанавливаем модули и запускаем сервер

```
npm install
npm run dev
```

- Заходим в папку client

```
cd client
```

- Устанавливаем модули и запускаем фронт

```
npm install
npm start
```
